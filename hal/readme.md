# Using docker-compose

* Create your .env file with: ./create-docker-compose-env
* Run docker-compose build && docker-compose up -d
* Run sudo ./restart-apache-container-when-needed

The last command runs a loop that stats the perl files and rebuilds and restarts the apache container when they change.

The .env file contains variables that are interpolated into the docker-compose.yaml file as documented here:
https://docs.docker.com/compose/environment-variables/set-environment-variables/

The default location to store the state of the dockerized setup is the docker subdirectory, it's ignored by
the restart-apache-container-when-needed script

# Restore a backup

PGPASSWORD=password123 pg_restore -h localhost -U hal -d hal /tmp/hal.Sunday-13.pg

PGPASSWORD=password123 pg_restore -c -h 172.19.0.2 -U hal -d hal /home/ff/projects/HAL900/hal-backups/hal.Wednesday-06.pg

# psql

PGPASSWORD=password123 psql -h 172.19.0.2 -U hal -d hal
