#pragma once
#include <stdint.h>

void initLEDs(void);
void setLED(uint8_t led, uint8_t on);
void setLEDs(uint8_t led);

void resetMsTimer(void);
void sleepUntilMs(uint8_t target);
